import custom_functions as f

MENU = "menu"
COMMAND = "command"
EXITMENU = "exitmenu"

menu_data = {
	'title': "Command Menu", 'type': MENU, 'subtitle': "Please select an option...",
	'options': [
		{'title': "Start Everything", 'type': COMMAND, 'command': {'f': f.start_everything, 'args': []}},
		{'title': "Project 1", 'type': MENU, 'subtitle': "What would you like to do?",
			'options': [
				{'title': "Start Project 1", 'type': COMMAND, 'command': {'f': f.start_project_one, 'args': []}},
				{'title': "Update Dependencies", 'type': COMMAND, 'command': {'f': f.update_dependencies, 'args': []}}
			]
			},
		{'title': "Project 2", 'type': MENU, 'subtitle': "What would you like to do?",
			'options': [
				{'title': "Start Project 2", 'type': COMMAND, 'command': {'f': f.start_project_two, 'args': []}},
				{'title': "Create new file", 'type': COMMAND, 'command': {'f': f.create_new_file, 'args': []}},
			]
			},
		{'title': "SSH", 'type': MENU, 'subtitle': "Which server would you like to SSH to?",
			'options': [
				{'title': "Server 1", 'type': COMMAND, 'command': {'f': f.ssh_to_address, 'args': ["username", "serverone.com", f.projectone_pem_path]}},
				{'title': "Server 2", 'type': COMMAND, 'command': {'f': f.ssh_to_address, 'args': ["username", "servertwo.com"]}}
			]
			}
	]
}
