import curses
import os
import signal
import time
from screen import screen

terminal_app = "iTerm"


def get_param(r, c, prompt_string):
	curses.echo()
	screen.addstr(r, c, prompt_string)
	screen.refresh()
	user_input = screen.getstr(r + 1, c)
	curses.noecho()
	return user_input


def kill_process(pid):
	os.kill(pid, signal.SIGHUP)


def is_process_running(name):
	for line in os.popen("ps xa"):
		fields = line.split()
		pid = fields[0]
		process = ' '.join(fields[4:])

		if name in process:
			return int(pid)
	return False


def start_program(cmds, new_tab, restart):
	pid = is_process_running(cmds[-1])
	if pid:
		if restart:
			kill_process(pid)
			start_process(cmds, new_tab)
	else:
		start_process(cmds, new_tab)


def start_process(cmds, new_tab):
	cmd = ';'.join(cmds)
	if new_tab:
		execute_new_tab(cmd)
	else:
		os.system(cmd)
	time.sleep(1)


def execute_new_tab(cmd_string):
	wrapped_cmd = """
	osascript &>/dev/null <<EOF
		tell application "{0}"
			tell current terminal
				launch session "Default Session"
				tell the last session
					write text "{1}"
				end tell
			end tell
		end tell
	EOF
	""".format(terminal_app, cmd_string)
	os.system(wrapped_cmd)
