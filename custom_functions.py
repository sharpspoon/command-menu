#!/usr/bin/env python

from core_functions import start_program, start_process, get_param

projectone_path = "~/projectone"
projecttwo_path = "~/projecttwo"
projectone_pem_path = "~/.ssh/sshkey.pem"



def create_new_file():
	filename = get_param(2, 3, "Name of File?")
	start_program(["cd {0}".format(projectone_path), "touch {0}".format(filename)], True, False)


def update_dependencies(project):
	start_program(["bower install"], True, False)


def ssh_to_address(user, server_name, pem_path=None):
	if pem_path:
		start_process(["ssh -i {0} {1}@{2}".format(pem_path, user, server_name)], True)
	else:
		start_process(["ssh {0}@{1}".format(user, server_name)], True)


def start_project_one():
	start_program(["cd {0}".format(projectone_path), "node projectone.js"], True, False)


def start_project_two():
	start_program(["cd {0}".format(projecttwo_path), "node projecttwo.js"], True, False)


def start_everything():
	start_project_one()
	start_project_two()
